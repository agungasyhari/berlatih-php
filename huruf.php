<?php
function ubah_huruf($string){
    $abjad = "abcdefghijklmnopqrstuvwxyz" ; 
    $result = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $position = strrpos ($abjad, $string[$i]);
        $result .= substr ($abjad, $position + 1, 1);
    } 
    return $result;
}

// TEST CASES
echo ubah_huruf('wow') . '<br>'; // xpx
echo ubah_huruf('developer') . '<br>'; // efwfmpqfs
echo ubah_huruf('laravel') . '<br>'; // mbsbwfm
echo ubah_huruf('keren') . '<br>'; // lfsfo
echo ubah_huruf('semangat') . '<br>'; // tfnbohbu

?>