<?php
function tentukan_nilai($number)
{
    $result ="";

    if ($number >= 85 && $number <= 100) {
        $result .= "Sangat Baik";
    } elseif ($number >= 70 && $number < 85) {
        $result .= "Baik";
    } elseif ($number >= 60 && $number < 75) {
        $result .= "Cukup"; 
    } else {
        $result .= "Kurang"    ;
    }
    return $result;
}

echo tentukan_nilai(98) . "<br>"; //Sangat Baik
echo tentukan_nilai(76) . "<br>"; //Baik
echo tentukan_nilai(67) . "<br>"; //Cukup
echo tentukan_nilai(43) . "<br>"; //Kurang


?>